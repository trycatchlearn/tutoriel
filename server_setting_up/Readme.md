# Préparer sa machine à accéder à un serveur distant.

## Environnement 

- Système d'exploitation :
    - Machine d'accès : `Windows`
    - Serveur distant : `Ubuntu 16.04 Server`
    - Hébergement : [OVH] - VPS SSD 2 (4 Go RAM, SSD 20 Go).
- Outils :
    - [PuTTY]
    - [PuTTY Tray]
    - [vcxsrv]
    - [SFTPNetDrive]

## Prérequis 

- Avoir un serveur, ou une machine distante (avec un serveur ssh d'installé) sur laquelle vous souhaitez travailler.
- Connaître l'adresse IP de votre serveur distant.

## Objectifs du tutoriel

- Configurer l'accès à un serveur distant depuis une machine Windows.
- Appréhender le concept d'authentification par clé et son utilisation. 
- Comprendre le principe de Virtual Agent.
- Utiliser les interfaces de mon serveur distant.
- Monter un volume de votre serveur.


## Contexte 

Aujourd'hui la mise en place d'une application web, ou réposant sur une couche web est indisociable de la notion de serveur. Bien qu vous puissiez développer votre applicatif et l'héberger sur votre machine en local, la nécessité de le rendre accessible par la suite au "monde entier" passera par le déploiement sur un serveur.
De la même façon dans de très nombreuses entreprises, l'essentiel de votre travail se fera sur une [VDI] et non pas directement sur votre machine locale. D'où la nécessité d'être en mesure de préparer proprement votre environnement de travail.

## Plan 

- I - Installation des outils nécessaires
- II - Connexion à votre serveur via PuTTY
- III - Authentification par clés, Agent et PuTTY Tray
- IV - Serveur X, vcxsrv
- V - Monter un volume vers son serveur.

### I - Installation des outis nécessaires

#### Téléchargement des outils
Pour commencer, nous allons télécharger l'ensemble des outils nécessaires.

[PuTTY] : il s'agit d'un émulateur de terminal couplé à un client pour les protocoles [SSH], [Telnet], [rlogin] et [TCP brut].

[PuTTY Tray]: est une version améliorée de [PuTTY], fournissant des ajouts pour rendre l'utilisation plus simple et utile. Dans notre cas, la gestion des Agents.

#### Installation de PuTTY

En suite on installe [PuTTY], on ajoute tous les outils qui sont proposés.

![Installation PuTTY choix des outils](images/I_Installation_outils/putty_installation_tools.png)

#### Installation de PuTTY Tray

Une fois cette étape réalisé, on peut mettre installer [PuTTY Tray], pour ça rien de plus simple on remplace le `putty.exe` présent dans votre répertoire d'installation PuTTY, par celui téléchargé sur [PuTTY Tray].

![Installation PuTTY Tray](images/I_Installation_outils/putty_tray_install.png)
![PuTTY Tray remplacement](images/I_Installation_outils/putty_tray_replace.png)

#### Lancer le client

On peut maintenant lancer le client [PuTTY] (qui est maintenant celui de [PuTTY Tray]).

![Putty Client](images/I_Installation_outils/putty_client.png)

### II - Connexion au serveur via PuTTY 

Nous voilà avec notre beau client [PuTTY], mais ça n'est pas tout ça mais on aimerait bien allez faire un petit tour sur notre serveur maintenant ... 

Dans l'onglet session à gauche 

![Arborescence Session](images/II_Connection_server_with_putty/session_onglet.png)

On commence par définir la destination de la connexion : 
- saisir l'adresse IP ou l'[hostname] du serveur.
- Choisir `SSH` comme type de connection

![Destination server](images/II_Connection_server_with_putty/desitnation_server.png)

Note : le port de connexion par défaut est le 22, si jamais un autre a été configurer sur votre serveur mettre celui correspondant.

Pour se simplifier la vie, se rendre dans l'onglet `Connection > Data`,

![Arborescence Data](images/II_Connection_server_with_putty/data_onglet.png)

puis remplir le champ `Auto-login username`, avec votre login de connexion sur le serveur, pour ne plus avoir à le taper à chaque connexion.

![Login détails](images/II_Connection_server_with_putty/login_details.png)

Avant de faire quoi que ce soit d'autre, on va sauvegarder nos paramètres, afin de ne pas devoir les saisir de nouveau à chaque connexion. 

![Save session](images/II_Connection_server_with_putty/save_session.png)

On remplit le champ avec le nom souhaité, puis on appuie sur `Save`.

![Saved session](images/II_Connection_server_with_putty/saved_session.png)

Notre session apparaît maintenant dans la liste, il suffira de la recharger via le bouton `Load` au besoin.

On peut maintenant se connecter à notre serveur en cliquant sur `Open`

![Clique sur le bouton Open](images/II_Connection_server_with_putty/open_clic.png)

Si c'est la première fois que vous vous rendez sur votre serveur avec PuTTY, vous devrez dire à PuTTY que vous faites confiance à la machine sur laquelle vous vous rendez.

![Trust host](images/II_Connection_server_with_putty/trust_host.png)

Vous vous retrouvez maintenant sur un terminal [PuTTY], où vous allez pouvoir saisir votre mot de passe (pas besoin du username, puisqu'on l'a déjà spécifié à [PuTTY]). 

![terminal login](images/II_Connection_server_with_putty/terminal_login.png)

Note : Si vous vous connectez pour la première fois à un serveur hébergé (Digital Ocean, OVh etc...), je vous conseille de créer un nouvel utilisateur et d'y affecter un mot de passe : 

```sh
# sudo adduser <username>
```

Puis de l'ajouter au groupe sudo (même droit que root):

```sh
# sudo adduser aguitton sudo
```

Déconnectez vous, modifier le nom d'utilisateur dans `Connection > Data` par celui que vous venez de créer.
Puis sauvegarder vos modification dans l'onglet `Session`, en cliquant sur le bouton `Save`.

### III - Authentification par clés, Agent et PuTTY Tray 

#### Authentification par clé
Toutes mes félicitations ! vous venez de vous connecter à votre serveur et êtes mainteant capable de commencer à travailler dessus. Mais bon disons le clairement devoir taper son mot de passe, à chaque fois que l'on se connecte sur le serveur ... C'est pas fou.

Heureusement il existe quelque chose pour nous sauver (et de sécurisé), l'authentification par clé ! Ce système permet de garantir à un système (ici notre serveur) qu'un utilisateur (vous) est bien celui qui prétend être.

Les deux composantes essentielles cette authentification sont : 
- `Une clé publique` : clé exportée sur chaque hôte auprès desquels ont va vouloir se connecter.
- `Une clé privée`: permettant de prouver son identité aux différents hôtes.

#### Génération d'une paire de clés

Si vous me suivez toujours, il nous faut deux clés ... Mais comment est-ce qu'on les créer ces clés, vous allez me demander. Et bien, rappelez-vous au moment d'installer [PuTTY] je vous avez dit d'installer tous les outils proposés, et bien celui qui va nous intéresser ici c'est `PuTTYgen`.

![PuTTYgen](images/III_Authentication_with_keys_agent/puttygen.png)

Donc lancons `PuTTYgen`: 

![PuTTYgen client](images/III_Authentication_with_keys_agent/puttyGen_client.png)

On va ensuite générer notre paire de clé, pour ça cliquer sur `Generate`, avec comme paramètre [RSA] et `2048`.

![generate rsa keys](images/III_Authentication_with_keys_agent/generate_keys.png)

**Note :** pour la génération des clés, l'outil à besoin d'[entropie], et pour ça il suffit de bouger sa sourie dans tous les sens dans la petite boxe `Key`.

![entropie](images/III_Authentication_with_keys_agent/entropie.png)

Et nous voilà avec une magninfique clé publique

![publick key](images/III_Authentication_with_keys_agent/public_key.png)

On va sauvegarder notre `private key`, petit rappel, c'est celle-ci qui va nous permettre de venir dire au serveur : "Voilà la preuve que je suis, qui je prétends être".

![Sauvegarder la clé privée](images/III_Authentication_with_keys_agent/save_private__key.png)

**Note :** Il est coutume de mettre les clé dans votre dossier `.ssh`, `C:\Users\{username}\.ssh`.

On ne sauvegarde pas la clé publique, puisqu'on peut la régénérer avec `PuTTYgen` en chargeant la clé privé.

![Charger la clé privée](images/III_Authentication_with_keys_agent/load_private_key.png)

**Note :** garder ouvert `PuTTYgen`, avec votre clé publique, on va en avoir besoin.

#### Ajout de la clé publique

Bon si je résume, pour le moment : 
- On peut se rendre sur notre serveur en saisissant notre mot de passe via PuTTYGen
- On a une paire de clés [RSA].

On va donc maintenant mettre la `clé publique` sur notre serveur. Ce qui équivaut à dire qu'on va dire à notre serveur : *"L'utilisateur qui possède la clé privée correpondante à notre clé publique à le droit d'être connecté"*.

###### Déposer la clé publique sur le serveur
On se connecte sur notre serveur avec PuTTY.
S'il n'existe pas déjà créer le répertoire `~/.ssh`, c'est ici que l'n va déposer notre clé publique.

```sh
$ mkdir ~/.ssh
```

On se place dans le dossier `~/.ssh`

```sh
$ cd ~/.ssh
```

On copie notre `clé publique`, qui est affichée dans PuTTYgen.

![publick key](images/III_Authentication_with_keys_agent/public_key.png)

On créer notre fichier contenant la `clé publique` (avec l'extension `.pub` ).

```sh
$ touch id_rsa.pub
```

Puis on colle le contenu dans le fichier créé. 

- Avec `vi` : 
1. 

```sh
$ vi id_rsa.pub
```

2. On appuie sur la `touche i`, pour entrer en mode `Insert`. 
3. On colle la clé avec le `clic droit de la souris`.
4. On appuie sur la `touche echap` pour sortir du mode `Insert`.
5. On fait `Maj+Z+Z` pour sortir de `vi`.


##### Déclarer la clé auprès du serveur

Voilà notre `clé publique` sur le serveur, maintenant il faut déclarer au server ssh que l'on accepte cette clé. 

On créer un nouveau fichier `authorized_keys`.

```sh
$ touch authorized_keys
```

On spécifie que celui qui possède le fichier, peut lire et écrire dans ce nouveau fichier.

```sh
$ chmod 600 authorized_keys
```

On va ensuite ajouter notre clé à ce fichier, pour que le serveur la prenne en compte lors de l'authentification.

```sh
$ cat ./id_rsa.pub >> authorized_keys
```

Nous n'avons plus besoin de notre fichier contenant la `clé publique`, donc on le supprime.

```sh
$ rm ./id_rsa.pub
```

Quittez la console.

#### Définir un agent

On va utiliser le `Pageant` que nous propose PuTTY Tray, il s'agit d'une Agent d'authentification SSH, qui permet de garder en mémoire une `clé privée`, déjà décodée, que vous pouvez donc utiliser autant que vous voulez sans avoir à rétaper la passphrase. Cela sera aussi très pratique pour la partie avec [vcxsrv].

Pour cela, cliquer sur le bouton `Agent` dans [PuTTY] (n'est présent que si vous utilisez [PuTTY Tray]).

![Agent PuTTY](images/III_Authentication_with_keys_agent/agent_putty.png)

Vous arrivez sur cette fenêtre.

![Agent PuTTY fenêtre](images/III_Authentication_with_keys_agent/putty_agent_window.png)

On va ajouter notre `clé privée` à l'agent, pour ça appuyer sur `Add Key`.

![Agent ajouter clé](images/III_Authentication_with_keys_agent/add_key_agent.png)

Sélectionnez votre `clé privée`, localisée (si vous avez suivi le tutoriel) dans le dossier `C:\Users\{username}\.ssh`.

Votre agent a maintenant votre `clé privée` dans sa liste. 

![Agent liste clés](images/III_Authentication_with_keys_agent/pageant_list_key.png)

On peut fermer la fenêtre du Pageant et [PuTTY].

Si vous regarder dans les icônes de votre barre, un nouvel élément est apparu.

![pageant icon](images/III_Authentication_with_keys_agent/pageant_icon.png)

Il s'agit d'un raccourci vers le Pageant de PuTTY Tray. 

Pour commencer faites un `clic droit` dessus, et cochez `Persist Keys` et `Start with Windows`.

![pageant menu](images/III_Authentication_with_keys_agent/pageant_menu.png)

#### Se connecter au serveur

Voilà tout est prêt maintenant pour que nous puissions nous connecter à notre serveur. 
Toujours sur le petit icône de `Pageant`, cette fois faites un `clic gauche` dessus. Une liste apparaît, il s'agit de la liste des sessions que vous avez sauvegardez.

![pageant liste sessions](images/III_Authentication_with_keys_agent/pageant_list_session.png)

Il vous suffit donc de cliquer sur la session correspondant à la connexion de votre serveur ... et magie vous voilà connecté sur votre serveur \o/.

Donc maintenant pour vous connecter à vos différents serveurs il vous suffira : 
1. d'allumer votre ordinateur,
2. de cliquer sur l'icône `Pageant`,
3. sélectionner la session à lancer.
4. et c'est tout !

### IV - Serveur X, vcxsrv

On a un accès à notre serveur tout beau tout propre, mais dans le cas de [VDI], en tout cas en entreprise, il arrive souvent que celle-ci soit notre espace de travail, et notre poste seulement une machine permettant d'y accéder. Donc autant vous dire que tout faire en ligne de commande va vite être très fatiguant. 

On va donc mettre en place un serveur X, ce qui va nous permettre d'avoir tous le travail qui sera fait par le serveur, et uniquement l'affichage à gérer par notre machine.

Pour cela, on va commencer par modifier un petit truc sur notre session [PuTTY].

#### Compression des données

Pour éviter que les données reçues par notre serveur soit trop lourdes on va les compressés : 
- Chargez votre session de travail,
- Rendez-vous dans l'onglet `SSH > Cipher`

![cipher onglet](images/IV_server_x_vcxsrv/cipher_onglet.png)

- Remontez les algorithme `Arcfour` et `Blowfish` dans la liste (qui sont les plus performant).

![cipher policy](images/IV_server_x_vcxsrv/ciper_policy.png)

- Sauvegarder les modifications.

#### Installation du serveur X

Téléchargez [vcxsrv], puis l'installer.

![vcxsrv installation](images/IV_server_x_vcxsrv/vcxsrv_install.png)

Une fois l'installation terminée, fermez la fenêtre. Puis ajouter [vcxsrv] aux applications qui se lancent au démarrage de votre ordinateur. Pour cela placer le raccourci de vers [vcxsrv] dans le dossier : `C:\Users\{username}\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup`


#### Ouvrir une application

Pour tester que tout fonctionne bien, on va tester d'ouvrir une application, ouvrir votre session (petit rappel : `clic gauche sur l'icône de Pageant` et on sélectionne dans la liste).

Si vous ne l'avez pas déjà, on installe `terminator` pour l'exemple : 

```sh
$ sudo apt-get update
$ sudo apt-get install terminator
```

Et on le lance.

```sh
$ terminator
```

Et si tout se passe bien votre terminal se lance ! (Ce qui est pratique c'est que votre [vxsrv] utilise automatiquement l'agent qui est en route ;D).

#### Accès rapide à nos applications

Pour éviter d'avoir à lancer depuis une console nos différentes applications, on va se simplifier la vie. 

On commence par créer une nouvelle session : `clic droit` sur l'icône de Pageant.

![Nouvelle session](images/IV_server_x_vcxsrv/new_session.png)

On charge notre session habituelle de travaille.

![Charge session](images/IV_server_x_vcxsrv/load_session.png)

On change le nom de la session, dans notre cas on veut pouvoir lancer `terminator` directement.

![session terminator](images/IV_server_x_vcxsrv/terminator_session.png)

Ensuite, on va préciser que l'on souhaite lancer la commande `terminator` au démarrage de la session. Pour ça on se rend dans l'onglet `SSH`.

![Arborescence SSH](images/IV_server_x_vcxsrv/ssh_onglet.png)

On spécifie la `remote command` à exécuter.

![Commande à exécuter](images/IV_server_x_vcxsrv/remote_command_terminator.png)

Si vous le souhaitez, vous pouvez changer l'icône d'affichage de la session, pour vous y retrouver ;), se rendre dans l'onglet `Window > Behaviour`.

![Onglet Behaviour](images/IV_server_x_vcxsrv/behaviour_onglet.png)

![changer icône terminal](images/IV_server_x_vcxsrv/ico_terminator.png)

On sauvegarde nos modifications, puis avec un petit `clic gauche` sur l'icône `Pageant`, on lance notre nouvelle session ... 

![lancer terminator](images/IV_server_x_vcxsrv/run_terminator.png)

Et magie notre `terminator` s'ouvre avec notre bel icône. Et notre machine ne s'occupe que de gérer l'affichage, et le serveur gère le reste !! Vous pouvez maintenant ajouter votre `chrome`, `firefox` etc..

### V - Monter un volume vers son serveur

Bon c'est bien beau tout ça, mais bien que travailler avec un serveur X est pratique dans bon nombre de situations, cela peut aussi avoir ses limites, notamment lorsqu'on travaille avec un IDE qui analyse en permanence votre contenu pour l'affichage. 

C'est là qu'on va pouvoir monter un volume ! Le principe, avoir un driver sur notre machine, qui va être mapper au dossier de notre serveur distant. L'intérêt : Avoir accès à tout nos dossier sur notre machine, et que les modifications soient répercutées sur les données du serveur. 

Il existe plusieurs solutions dont : 
- [win-sshfs]
- [SFTPNetDrive]

La solution que j'ai retenu est la seconde, qui pour avoir traité les deux (et beaucoup d'autres solutions) et la plus souple et réactive.

#### Installation

On commence donc par installer [SFTPNetDrive].

![SFTPNetDrive installation](images/V_mount_volume/sftpnet_drive_install.png)

On lance [SFTPNetDrive].

![SFTPNetDrive lancement](images/V_mount_volume/sftpnetdrive_launch.png)

On créer un nouveau profil

![SFTPNetDrive nouveau profile](images/V_mount_volume/sftpnetdrive_new_profile.png)

![SFTPNetDrive création profile](images/V_mount_volume/sftpnet_drive_create_new_Profile.png)

Note : en version gratuite vous pouvez seulement remplir le champ `Host name or IP address`.

Puis on finit de configurer le profile, avec le `User Name` et l' `Authentification` (avec le mot de passe ou notre clé privée).
![SFTPNetDrive configuration](images/V_mount_volume/sftpnetdrive_configuration.png)

Ne reste plus qu'à appuyer sur le bouton `Connect`. Et voilà tout est prêt pour pouvoir travailler dans de bonnes conditions !

[SFTPNetDrive]: <http://www.01net.com/telecharger/windows/Internet/ftp/fiches/130045.html>
[PuTTY]: <https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html> 
[PuTTY Tray]: <https://puttytray.goeswhere.com/>
[vcxsrv]: <https://sourceforge.net/projects/vcxsrv/?SetFreedomCookie>
[clés SSH]: <https://fr.wikipedia.org/wiki/Secure_Shell>
[OVH]: <https://www.ovh.com/fr/>
[VDI]: <https://fr.wikipedia.org/wiki/VDI>
[SSH]: <https://fr.wikipedia.org/wiki/Secure_Shell>
[Telnet]: <https://fr.wikipedia.org/wiki/Telnet>
[rlogin]: <https://fr.wikipedia.org/wiki/Rlogin>
[TCP brut]: <https://fr.wikipedia.org/wiki/Transmission_Control_Protocol>
[hostname]: <https://technique.arscenic.org/reseau/article/noms-d-hote-hostname-et-noms-de>
[RSA]: <https://fr.wikipedia.org/wiki/Chiffrement_RSA>
[entropie]: <https://fr.wikipedia.org/wiki/Entropie>